# JS Pain Code Deploy - A Bash Retake exercise 

Welcome to the second exercise on Bash - A Retake exercise! In this this exercise you'll create a script to install and deploy JS Pain! Amazing!

## Topics covered

- Git & Github/bitbucket
- Good documentation
- Bash scripting
- SSH keys and comands
- AWS Basics

## Task

We need JS Pain to be able to run even if they take it down! We need a script to install JS Pain.

Here is the Software: 
- https://github.com/1j01/jspaint

Figure out how to get it in the machine and install it.

Also I want to have a Bastion, so it can enter it securely. 

This can be a seperate script or setup.

I also want a good documentation so I can follow the step by step.

Not everything can be automated? cool, put it in the documentation.

### Deliverables:

- Scrip to setup JSpaint 
- documentation of setup 
- Working example wtih bastion setup & correct tools