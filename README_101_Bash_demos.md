# Bash Exercises and Demos - A Bash Retake exercise 

Welcome to Bash exercises and Demos! This one is to consilidate on some key parts of bash and operating system that are important. 

What we are looking for: 

- Good documentation
- Small demo code
- You delivering the small demos 

For this exercise you DO NOT NEED SCRIPTs as a deliverable. It doesn't mean you can't use them, by all means do! It is at your descrtion. For somepart I think scripts are best. 

## Task 

Your task is to explain concepts and give a demo. 

I will give a list of topic and for each you need to have:

- Short description 
- syntax
- Demo Code
- Instructions 

### Acceptance criterea

- Is it's own repo
- is git enabled
- at least 5 commits 
- Documentation includes: Title, Intro, Pre-Requisits, Main content
- All of the topic are addressed
- All of the topics have a Description, Demo Code and Instructions

## Topics

This is the list of topic you need to cover:

- **Bash Variables**
- **Piping & Redirection**
- **Wild card and Matching**
- **grep and ps aux**


On the **Bash Variables**:

- How to set Variables
- How to access them
- How what is a child process
- how to set variables at environment level


On **Piping & Redirection**:

- What is piping?
- what is redirecting? 
- When are they useful?


On **Wild card and Matching**

- What are wild cards?


On **grep and ps aux**
- Make a process to sleep for 10 seconds using the command `sleep`, send it to the background using the `&` The can you use 
- use ps aux with grep to find the process
- kill it.