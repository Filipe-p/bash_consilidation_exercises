# This is the Assessment 1 Consilitation Exercise

This repo outlines an excercise to validate and consilidate your understanding of Bash and Cloud Technologies.

## Introduction

The tasks here are to give you a chance to validate and consilidate your knowledge. The has two parts:

1) Bash exercises and demo - see `README_101_Bash_demos.md`
2) An exercise to make build Js Pain - see `README_102_JS_PAINT.md`


## DoD - Definition of Done

- Each Task is it's own Private Repo on Bitbucket
- You have given your Instructor Access to repo
- All the Acceptance critereon are met
- Unless otherwise indicated - there is a script

### Topics covered

- Git & Github/bitbucket
- Good documentation
- Bash scripting & system ops
- Bash topics:
  - Piping & Redirection
  - Symbolic Links
  - Creating users
  - Searching Tasks and killing them
